import { Grid, Skeleton, Box, Typography } from '@mui/material'

import Card from '@mui/material/Card'
import { styled } from '@mui/material/styles'
import { AvatarProps } from '@mui/material/Avatar'
import CardContent from '@mui/material/CardContent'
import CustomAvatar from 'src/@core/components/mui/avatar'
const AppointmentCard = () => {
  const Avatar = styled(CustomAvatar)<AvatarProps>(({ theme }) => ({
    width: 40,
    height: 40,
    marginRight: theme.spacing(4)
  }))

  return (
    <>
      <Grid container spacing={3} className='match-height'>
        <Grid item xs={12} sm={6} md={3} lg={3} sx={{ height: '150px', mb: 5 }}>
          <Card>
            <CardContent>
              <Grid item xs={12}>
                <Skeleton sx={{ height: '3rem' }} animation='wave' />
              </Grid>
              <Box
                sx={{
                  display: 'flex',
                  alignItems: 'flex-start',
                  justifyContent: 'space-between'
                }}
              ></Box>
              <Grid item xs={12}>
                <Skeleton sx={{ height: '1.7rem' }} animation='wave' />
              </Grid>
              <Box>
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Typography
                    variant='h6'
                    sx={{ fontWeight: '2rem' }}
                  ></Typography>
                </Box>
              </Box>
              <Grid item xs={12}>
                <Skeleton sx={{ height: '1.7rem' }} animation='wave' />
              </Grid>
              <Box>
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Typography
                    variant='h6'
                    sx={{ fontWeight: '2rem' }}
                  ></Typography>
                </Box>
              </Box>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} sm={6} md={3} lg={3} sx={{ height: '150px', mb: 5 }}>
          <Card>
            <CardContent>
              <Grid item xs={12}>
                <Skeleton sx={{ height: '3rem' }} animation='wave' />
              </Grid>
              <Box
                sx={{
                  display: 'flex',
                  alignItems: 'flex-start',
                  justifyContent: 'space-between'
                }}
              ></Box>
              <Grid item xs={12}>
                <Skeleton sx={{ height: '1.7rem' }} animation='wave' />
              </Grid>
              <Box>
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Typography
                    variant='h6'
                    sx={{ fontWeight: '2rem' }}
                  ></Typography>
                </Box>
              </Box>
              <Grid item xs={12}>
                <Skeleton sx={{ height: '1.7rem' }} animation='wave' />
              </Grid>
              <Box>
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Typography
                    variant='h6'
                    sx={{ fontWeight: '2rem' }}
                  ></Typography>
                </Box>
              </Box>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} sm={6} md={3} lg={3} sx={{ height: '150px', mb: 5 }}>
          <Card>
            <CardContent>
              <Grid item xs={12}>
                <Skeleton sx={{ height: '3rem' }} animation='wave' />
              </Grid>
              <Box
                sx={{
                  display: 'flex',
                  alignItems: 'flex-start',
                  justifyContent: 'space-between'
                }}
              ></Box>
              <Grid item xs={12}>
                <Skeleton sx={{ height: '1.7rem' }} animation='wave' />
              </Grid>
              <Box>
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Typography
                    variant='h6'
                    sx={{ fontWeight: '2rem' }}
                  ></Typography>
                </Box>
              </Box>
              <Grid item xs={12}>
                <Skeleton sx={{ height: '1.7rem' }} animation='wave' />
              </Grid>
              <Box>
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Typography
                    variant='h6'
                    sx={{ fontWeight: '2rem' }}
                  ></Typography>
                </Box>
              </Box>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} sm={6} md={3} lg={3} sx={{ height: '150px', mb: 5 }}>
          <Card>
            <CardContent>
              <Grid item xs={12}>
                <Skeleton sx={{ height: '3rem' }} animation='wave' />
              </Grid>
              <Box
                sx={{
                  display: 'flex',
                  alignItems: 'flex-start',
                  justifyContent: 'space-between'
                }}
              ></Box>
              <Grid item xs={12}>
                <Skeleton sx={{ height: '1.7rem' }} animation='wave' />
              </Grid>
              <Box>
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Typography
                    variant='h6'
                    sx={{ fontWeight: '2rem' }}
                  ></Typography>
                </Box>
              </Box>
              <Grid item xs={12}>
                <Skeleton sx={{ height: '1.7rem' }} animation='wave' />
              </Grid>
              <Box>
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Typography
                    variant='h6'
                    sx={{ fontWeight: '2rem' }}
                  ></Typography>
                </Box>
              </Box>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </>
  )
}

export default AppointmentCard
