import { Grid, Skeleton, Box, Typography } from '@mui/material'

import Card from '@mui/material/Card'
import { styled } from '@mui/material/styles'
import { AvatarProps } from '@mui/material/Avatar'
import CardContent from '@mui/material/CardContent'
import CustomAvatar from 'src/@core/components/mui/avatar'

const HeaderProfile = () => {
  const Avatar = styled(CustomAvatar)<AvatarProps>(({ theme }) => ({
    width: 40,
    height: 40,
    marginRight: theme.spacing(4)
  }))

  return (
    <Box>
      <Card>
        <CardContent sx={{ py: theme => `${theme.spacing(4.125)} !important` }}>
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Avatar
              sx={{
                width: '80px',
                height: '80px',
                backgroundColor: 'rgba(76, 78, 100, 0.11)'
              }}
              src=''
            />
            <Box
              sx={{
                width: '100%',
                display: 'flex',
                flexWrap: 'wrap',
                alignItems: 'center',
                justifyContent: 'space-between'
              }}
            >
              <Box
                sx={{
                  mr: 2,
                  width: '30%',
                  display: 'flex',
                  mb: 0.4,
                  flexDirection: 'column'
                }}
              >
                <Typography
                  sx={{
                    ml: 1,
                    color: '#000',
                    fontWeight: '400',
                    fontSize: '24px'
                  }}
                >
                  <Grid item xs={12}>
                    <Skeleton sx={{ mb: 2 }} animation='wave' />
                  </Grid>
                </Typography>
                <Typography
                  sx={{
                    ml: 1,
                    color: '#7B88A2',
                    fontSize: '16px',
                    fontWeight: '400'
                  }}
                >
                  <Grid item xs={12}>
                    <Skeleton sx={{ width: '50%', mb: 2 }} animation='wave' />
                  </Grid>
                </Typography>
                <Typography sx={{ mt: 2 }}>
                  {' '}
                  <Grid item xs={12}>
                    <Skeleton sx={{ width: '50%', mb: 2 }} animation='wave' />
                  </Grid>
                </Typography>
              </Box>
            </Box>
          </Box>
        </CardContent>
      </Card>
    </Box>
  )
}

export default HeaderProfile
