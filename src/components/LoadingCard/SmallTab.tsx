import { Grid, Skeleton, Box, Typography } from '@mui/material'

import Card from '@mui/material/Card'
import { styled } from '@mui/material/styles'
import { AvatarProps } from '@mui/material/Avatar'
import CardContent from '@mui/material/CardContent'
import CustomAvatar from 'src/@core/components/mui/avatar'

const SmallTab = () => {
  const Avatar = styled(CustomAvatar)<AvatarProps>(({ theme }) => ({
    width: 40,
    height: 40,
    marginRight: theme.spacing(4)
  }))

  return (
    <Box>
      <Grid container spacing={6}>
        <Grid item xs={12} md={3} sm={6}>
          <CardContent
            sx={{
              backgroundColor: '#FFFFFF',
              py: theme => `${theme.spacing(4.125)} !important`
            }}
          >
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Avatar
                variant='rounded'
                sx={{
                  backgroundColor: 'rgba(76, 78, 100, 0.11)',
                  width: '2.5rem',
                  height: '2.5rem'
                }}
              ></Avatar>
              <Grid item xs={12}>
                <Skeleton sx={{ mb: 2 }} animation='wave' />
              </Grid>
              <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                <Box
                  sx={{
                    display: 'flex',
                    flexWrap: 'wrap',
                    alignItems: 'center'
                  }}
                ></Box>
              </Box>
            </Box>
          </CardContent>
        </Grid>
        <Grid item xs={12} md={3} sm={6}>
          <CardContent
            sx={{
              backgroundColor: '#FFFFFF',
              py: theme => `${theme.spacing(4.125)} !important`
            }}
          >
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Avatar
                variant='rounded'
                sx={{
                  backgroundColor: 'rgba(76, 78, 100, 0.11)',
                  width: '2.5rem',
                  height: '2.5rem'
                }}
              ></Avatar>
              <Grid item xs={12}>
                <Skeleton sx={{ mb: 2 }} animation='wave' />
              </Grid>
              <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                <Box
                  sx={{
                    display: 'flex',
                    flexWrap: 'wrap',
                    alignItems: 'center'
                  }}
                ></Box>
              </Box>
            </Box>
          </CardContent>
        </Grid>
        <Grid item xs={12} md={3} sm={6}>
          <CardContent
            sx={{
              backgroundColor: '#FFFFFF',
              py: theme => `${theme.spacing(4.125)} !important`
            }}
          >
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Avatar
                variant='rounded'
                sx={{
                  backgroundColor: 'rgba(76, 78, 100, 0.11)',
                  width: '2.5rem',
                  height: '2.5rem'
                }}
              ></Avatar>
              <Grid item xs={12}>
                <Skeleton sx={{ mb: 2 }} animation='wave' />
              </Grid>
              <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                <Box
                  sx={{
                    display: 'flex',
                    flexWrap: 'wrap',
                    alignItems: 'center'
                  }}
                ></Box>
              </Box>
            </Box>
          </CardContent>
        </Grid>
        <Grid item xs={12} md={3} sm={6}>
          <CardContent
            sx={{
              backgroundColor: '#FFFFFF',
              py: theme => `${theme.spacing(4.125)} !important`
            }}
          >
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Avatar
                variant='rounded'
                sx={{
                  backgroundColor: 'rgba(76, 78, 100, 0.11)',
                  width: '2.5rem',
                  height: '2.5rem'
                }}
              ></Avatar>
              <Grid item xs={12}>
                <Skeleton sx={{ mb: 2 }} animation='wave' />
              </Grid>
              <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                <Box
                  sx={{
                    display: 'flex',
                    flexWrap: 'wrap',
                    alignItems: 'center'
                  }}
                ></Box>
              </Box>
            </Box>
          </CardContent>
        </Grid>
      </Grid>
    </Box>
  )
}

export default SmallTab
