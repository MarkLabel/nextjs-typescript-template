import * as React from 'react'
import { styled, alpha, useTheme } from '@mui/material/styles'
import Button from '@mui/material/Button'
import Menu, { MenuProps } from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import { MenuDown, Plus } from 'mdi-material-ui'
import { Typography } from '@mui/material'

type Props = {
  title: React.ReactElement | string
  options: Option[]
}
export type Option = {
  title: string | React.ReactElement
  icon?: React.ReactElement
  callBack?: () => void
  closeWhenClicked: boolean
}

const StyledMenu = styled((props: MenuProps) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'right'
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'right'
    }}
    {...props}
  />
))(({ theme }) => ({
  '& .MuiPaper-root': {
    borderRadius: 6,
    marginTop: theme.spacing(1),
    minWidth: 180,
    color:
      theme.palette.mode === 'light'
        ? 'rgb(55, 65, 81)'
        : theme.palette.grey[300],
    boxShadow:
      'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px',
    '& .MuiMenu-list': {
      padding: '4px 0'
    },
    '& .MuiMenuItem-root': {
      '& .MuiSvgIcon-root': {
        fontSize: 18,
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(1.5)
      },
      '&:active': {
        backgroundColor: alpha(
          theme.palette.primary.main,
          theme.palette.action.selectedOpacity
        )
      }
    }
  }
}))

export default function DropdownMenu({ title, options }: Props) {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const open = Boolean(anchorEl)
  const theme = useTheme()

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <div>
      <Button
        id='dropdown-menu'
        aria-controls={open ? 'dropdown-menu-customized' : undefined}
        aria-haspopup='true'
        aria-expanded={open ? 'true' : undefined}
        variant='outlined'
        color='warning'
        disableElevation
        onClick={handleClick}
        endIcon={<MenuDown />}
        startIcon={<Plus />}
        sx={{
          borderWidth: '0.5px',
          backgroundColor: '#FFAF37',
          color: 'white',
          '&:hover': {
            color: '#FFAF37',
            backgroundColor: 'white',
            borderColor: '#FFAF37'
          }
        }}
      >
        {title}
      </Button>
      <StyledMenu
        id='dropdown-menu-customized'
        MenuListProps={{
          'aria-labelledby': 'dropdown-menu'
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
      >
        {options?.map(option => (
          <MenuItem
            key={`dropdown_menu_${title}_${option.title}`}
            onClick={() => {
              if (option.callBack) option.callBack()
              if (option.closeWhenClicked) handleClose()
            }}
          >
            {option?.icon}
            {option.title}
          </MenuItem>
        ))}
      </StyledMenu>
    </div>
  )
}
