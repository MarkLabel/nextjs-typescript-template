const Rectangle = () => {
  const logoStyles = {
    width: '4px',
    height: '1.5rem',
    marginRight: '10px'
  }
  return <img src='/images/Rectangle.png' style={logoStyles} />
}

export default Rectangle
