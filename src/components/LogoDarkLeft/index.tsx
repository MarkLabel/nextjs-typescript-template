const Logo = () => {
  const logoStyles = {
    width: '30px',
    height: '30px'
  }
  return <img src='/images/logo-left.png' style={logoStyles} />
}

export default Logo
