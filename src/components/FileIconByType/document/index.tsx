import Avatar from '@mui/material/Avatar'
import { red, yellow, green, blue } from '@mui/material/colors'

// ** icons
import { FilePdfBox, Folder, FileDocument, FileImage } from 'mdi-material-ui'

type Props = {
  type: string
}

const RenderFileIconByType = ({ type }: Props) => {
  switch (type) {
    case 'application/pdf':
      return (
        <Avatar
          variant='rounded'
          sx={{
            fontSize: '4rem',
            width: 100,
            height: 100,
            p: 0,
            bgcolor: 'transparent',
            color: red[400],
            transform: 'scale(4)',
            cursor: 'pointer'
          }}
        >
          <FilePdfBox />
        </Avatar>
      )
    case 'folder':
      return (
        <Avatar
          variant='rounded'
          sx={{
            fontSize: '4rem',
            width: 100,
            height: 100,
            p: 0,
            bgcolor: 'transparent',
            color: yellow[700],
            transform: 'scale(4)'
          }}
        >
          <Folder />
        </Avatar>
      )
    case 'image/png':
      return (
        <Avatar
          variant='rounded'
          sx={{
            fontSize: '4rem',
            width: 100,
            height: 100,
            p: 0,
            bgcolor: 'transparent',
            color: blue[900],
            transform: 'scale(4)'
          }}
        >
          <FileImage />
        </Avatar>
      )
    case 'image/jpeg':
      return (
        <Avatar
          variant='rounded'
          sx={{
            fontSize: '4rem',
            width: 100,
            height: 100,
            p: 0,
            bgcolor: 'transparent',
            color: blue[900],
            transform: 'scale(4)'
          }}
        >
          <FileImage />
        </Avatar>
      )
    case 'image/jpg':
      return (
        <Avatar
          variant='rounded'
          sx={{
            fontSize: '4rem',
            width: 100,
            height: 100,
            p: 0,
            bgcolor: 'transparent',
            color: blue[900],
            transform: 'scale(4)'
          }}
        >
          <FileImage />
        </Avatar>
      )
    default:
      return (
        <Avatar variant='rounded'>
          <FileDocument />
        </Avatar>
      )
  }
}

export default RenderFileIconByType
