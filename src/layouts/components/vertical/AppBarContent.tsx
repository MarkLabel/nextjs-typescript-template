// ** MUI Imports
import { Grid, Typography, IconButton, Box, Button } from '@mui/material'
import { useRouter } from 'next/router'

// ** Icon Imports
import Icon from 'src/@core/components/icon'

import { useEffect, useState } from 'react'
import authConfig from 'src/configs/auth'

// ** Type Import
import { Settings } from 'src/@core/context/settingsContext'
import LanguageDropdown from 'src/@core/layouts/components/shared-components/LanguageDropdown'

// ** Components
import ModeToggler from 'src/@core/layouts/components/shared-components/ModeToggler'
import UserDropdown from 'src/layouts/components/UserDropdown'

interface Props {
  hidden: boolean
  settings: Settings
  toggleNavVisibility: () => void
  saveSettings: (values: Settings) => void
}

const AppBarContent = (props: Props) => {
  // ** Props
  const router = useRouter()
  const { hidden, settings, saveSettings, toggleNavVisibility } = props
  const [companyActiveName, setCompanyActiveName] = useState<string>('')

  const getPageHome = () => {
    router.push(`/dashboard`)

  }

  useEffect(() => {
    const userDataStorage = window.localStorage.getItem(
      authConfig.storageUserData
    )
      ? JSON.parse(
        window.localStorage.getItem(authConfig.storageUserData) ?? ''
      )
      : ''
    const companyName = userDataStorage.companyName

    setCompanyActiveName(companyName)
  }, [])

  return (
    <Box
      sx={{
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#FFF',
        pl: '1rem',
        pr: '1rem',
        pt: '0.5rem',
        pb: '0.5rem',
        mt: '1rem',
        borderRadius: '10px'
      }}
    >
      <Box
        className='actions-left'
        sx={{ mr: 2, display: 'flex', alignItems: 'center' }}
      >
        {hidden ? (
          <IconButton
            color='inherit'
            sx={{ ml: -2.75 }}
            onClick={toggleNavVisibility}
          >
            <Icon icon='mdi:menu' />
          </IconButton>
        ) : null}
        <Grid
          display='flex'
          flexDirection='row'
          justifyContent='start'
          alignItems='center'
          gap={2}
        >
          <Typography
            variant='body1'
            sx={{
              fontWeight: 'bold',
              color: '#bababa',
              opacity: 0.9,
              border: '0px solid #cecece',
              padding: '2px 0px',
              borderRadius: '0.25rem',
              '@media (max-width: 767px)': {
                maxWidth: '100%',
                overflow: 'hidden'
              }
            }}
          >
            <Button onClick={getPageHome} variant="contained">Home</Button>
          </Typography>
        </Grid>
      </Box>
      <Box
        className='actions-right'
        sx={{ display: 'flex', alignItems: 'center' }}
      >
        <LanguageDropdown settings={settings} saveSettings={saveSettings} />

      </Box>
    </Box>
  )
}

export default AppBarContent
