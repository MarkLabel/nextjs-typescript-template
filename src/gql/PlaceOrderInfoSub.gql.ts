import { gql } from '@apollo/client'

export const gqlPlaceOrderInfoSub = gql`
  subscription PlaceOrderInfo($policyId: Int!) {
    placeOrderInfo(policyId: $policyId) {
      policyId
      bidCoverages {
        amount
        price
        orders {
          amount
          fee
          orderId
          orderType
          policyId
          price
          updated
          user
        }
      }
      bidPremiums {
        amount
        price
        orders {
          amount
          fee
          orderId
          orderType
          policyId
          price
          updated
          user
        }
      }
      offerCoverages {
        amount
        price
        orders {
          amount
          fee
          orderId
          orderType
          policyId
          price
          updated
          user
        }
      }
      offerPremiums {
        amount
        price
        orders {
          amount
          fee
          orderId
          orderType
          policyId
          price
          updated
          user
        }
      }
    }
  }
`
