// ** React Imports
import { ReactNode } from 'react'

// ** Layout Import
import BlankLayout from 'src/@core/layouts/BlankLayout'

// ** React Imports
import { useEffect } from 'react'

// ** Next Imports
import { useRouter } from 'next/router'

// ** Spinner Import
import Spinner from 'src/@core/components/spinner'

// ** Hook Imports

/**
 *  Set Home URL based on User Roles
 */
export const getHomeRoute = (role: string) => {
  return '/dashboard'
}

const Home = () => {
  // ** Hooks

  const router = useRouter()

  useEffect(() => {
    router.replace('/dashboard')

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  // return <Spinner />
}
Home.getLayout = (page: ReactNode) => <BlankLayout>{page}</BlankLayout>
export default Home
