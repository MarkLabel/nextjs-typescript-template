import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  split,
  HttpLink
} from '@apollo/client'
import { ReactNode } from 'react'
import { GraphQLWsLink } from '@apollo/client/link/subscriptions'
import { getMainDefinition } from '@apollo/client/utilities'
import { createClient } from 'graphql-ws'

import env from 'src/configs/env'

interface Props {
  children: ReactNode
}

export const ApolloServiceProvider = ({ children }: Props) => {
  const wsLink =
    typeof window !== 'undefined'
      ? new GraphQLWsLink(
          createClient({
            url: env.WS_GRAPHQL_URL
          })
        )
      : null

  const httpLink = new HttpLink({
    uri: env.GRAPHQL_URL
  })

  const link =
    typeof window !== 'undefined' && wsLink != null
      ? split(
          ({ query }) => {
            const def = getMainDefinition(query)

            return (
              def.kind === 'OperationDefinition' &&
              def.operation === 'subscription'
            )
          },
          wsLink,
          httpLink
        )
      : httpLink

  const client = new ApolloClient({
    link,
    cache: new InMemoryCache()
  })

  return <ApolloProvider client={client}>{children}</ApolloProvider>
}
