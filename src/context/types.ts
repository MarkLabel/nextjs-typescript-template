export type ErrCallbackType = (err: { [key: string]: string }) => void

export type LoginParams = {
  username: string
  password: string
}

export type UserDataType = {
  id: number
  role: string
  email: string
  fullName: string
  username: string
  password: string
  avatar?: string | null
}

export type IGetUserResponse = {
  id: number
  uuid: string
  // role: string
  email: string
  firstName: string
  lastName: string
  // username: string
  // password: string
  status: string
  createdAt: string
  updatedAt: string
}

export interface ILoginResponse {
  id: number
  uuid: string
  role: string
  email: string
  firstName: string
  lastName: string
  username: string
  password: string
  status: string
  createdAt: string
  updatedAt: string
  token: string
}

export interface UpdatePassword {
  currentPassword: string
  newPassword: string
}

export type AuthValuesType = {
  loading: boolean
  logout: () => void
  user: IGetUserResponse | null
  setLoading: (value: boolean) => void
  setUser: (value: IGetUserResponse | null) => void
  login: (params: LoginParams, errorCallback?: ErrCallbackType) => void
  userToken: string | null
  updatePassword: (
    params: UpdatePassword,
    errorCallback?: ErrCallbackType
  ) => void
}
