import Divider from '@mui/material/Divider'
import Box from '@mui/material/Box'
import Rectangle from 'src/components/Rectangle'
import Typography from '@mui/material/Typography'
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import Button from '@mui/material/Button';
import { useTranslation } from 'react-i18next'
import { useRouter } from 'next/router'

const Dashboard = () => {
  //สำหรับกำหนด route ของ proejct หรือสั่งให้ไปหน้านั้นๆ
  const router = useRouter()
  const getPage = (page: string) => {
    //รับค่าจาก function getPage onclick เพื่อกำหนด route ที่จะไป
    router.push(`/example/${page}`)
  }

  return (
    <>
      <Card>
        <Typography sx={{ p: 5 }}>GET </Typography>
        <Grid container spacing={4} sx={{ p: 5 }}>
          <Grid item xs={2}>
            <Button onClick={() => getPage('main')} color="success" variant="contained">MAIN</Button>
          </Grid>
        </Grid>
      </Card >
    </>
  )
}

export default Dashboard
