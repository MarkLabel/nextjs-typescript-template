import { Card, Box } from '@mui/material';
const Example = () => {
  return (
    <>
      <Card>
        <Box sx={{ p: 10 }}>
          Example Components
        </Box>
      </Card >
    </>
  )
}

export default Example
