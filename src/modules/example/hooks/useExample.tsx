import http from 'src/libs/axios'
import { apiEndPoint } from 'src/modules/example/constants'
export const useExample = () => {

  const getExample = async () => {
    try {
      const response = await http.get(
        `${apiEndPoint.example}`,
      )
      const data = response?.data
      return data
    } catch (error) {
      return []
    }
  }


  return {
    getExample,
  }
}
