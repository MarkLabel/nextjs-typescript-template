export default {
  storageTokenKeyName: 'accessToken',
  storageUserData: 'userData',
  onTokenExpiration: 'refreshToken',
  expirationTimeToken: 'expirationTimeToken',
  refreshTokenKeyName: 'refreshToken',
  firebaseUser: 'firebaseUser'
}
