import { useTranslation } from 'react-i18next'
import dayjs, { Dayjs } from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'

dayjs.extend(relativeTime)
interface dateType {
  date: Date
  format?: string | 'YYYY-MM-DD HH:mm'
}
export const dayFormatter = (args: dateType) => {
  const { t } = useTranslation()

  const localeObject = {
    defaultLocale: 'en',
    name: t('lang'),
    weekdays: t('dayjsObject.day').split('_'),
    weekdayShort: t('dayjsObject.dayShort').split('_'),
    months: t('dayjsObject.months').split('_'),
    monthsShort: t('dayjsObject.monthsShort').split('_')
  }

  const newDate = dayjs(args.date)
    .locale(`${t('lang')}`, localeObject)
    .format(args.format ?? 'YYYY-MM-DD HH:mm')
  return newDate as string
}

export const prettyRelativeDate = (inputDate: string) => {
  const now = dayjs()
  const date = dayjs(inputDate)

  // Calculate the difference in days between now and the input date
  const daysDifference = now.diff(date, 'day')

  if (daysDifference < 1) {
    // Less than 1 day ago, format as "few minutes ago" or "10:00 pm"
    if (now.diff(date, 'minute') <= 1) {
      return 'few minutes ago'
    } else {
      return date.format('h:mm A')
    }
  } else if (daysDifference < 7) {
    const relativeTime = date.fromNow(true)
    return relativeTime
  }

  return date.format('DD/MM/YYYY')
}
