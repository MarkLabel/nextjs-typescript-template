import Axios from 'axios'

import env from 'src/configs/env'
const gatewayApi = Axios.create({
  baseURL: env.API_BASE_URL
})

export default gatewayApi
