import React, { ChangeEvent } from 'react'
import router, { useRouter } from 'next/router'
import { t } from 'i18next'
import IconButton from '@mui/material/IconButton'
import { Box, Grid, Button, Breadcrumbs, Link, TextField } from '@mui/material'
import { styled, useTheme } from '@mui/material/styles'
import * as _ from 'lodash'
import { useTranslation } from 'react-i18next'
import { Magnify, Close, Plus } from 'mdi-material-ui'
import ChevronRightIcon from 'src/@core/components/design-ui/icons/ChevronRightIcon'

type Props = {
  toolbarSearchValue: string
  onMutationToolbarSearchValue: (updated: string) => void
  onSubmitToolbarButton: (updated: string) => void
  onChange: (e: ChangeEvent<HTMLInputElement>) => void
  children?: any
}

const UiDataGridCustom = (props: Props) => {
  const theme = useTheme()
  const { t } = useTranslation()
  return (
    <Grid item xs={12}>
      <Grid
        container
        padding={0}
        display='flex'
        flexDirection='row'
        justifyContent='space-between'
        alignItems='center'
        className='relative rounded-lg w-full'
        sx={{
          paddingY: '1rem'
        }}
      >
        <Grid item></Grid>
        <Grid item display='flex'>
          <TextField
            size='small'
            sx={{ '& .MuiInputBase-root': { gap: '.5rem' } }}
            onChange={props.onChange}
            value={props.toolbarSearchValue}
            placeholder={t('TextField.search') as string}
            InputProps={{
              startAdornment: <Magnify fontSize='small' />,
              endAdornment: (
                <IconButton
                  size='small'
                  title='clear'
                  aria-label='clear'
                  onClick={() => props.onMutationToolbarSearchValue('')}
                >
                  <Close fontSize='small' />
                </IconButton>
              )
            }}
          />
        </Grid>
      </Grid>
    </Grid>
  )
}

export default UiDataGridCustom
