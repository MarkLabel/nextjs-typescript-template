import { useTheme } from '@mui/material/styles'

type SvgIconProps = {
  dimension?: number
  color?: string
}

const TaskSuitcaseIcon = (props: SvgIconProps) => {
  const theme = useTheme()
  const dimension = props.dimension ?? 24
  const color = props.color ?? theme.palette.grey[600]

  return (
    <svg
      width={dimension}
      height={dimension}
      viewBox={`0 0 ${dimension} ${dimension}`}
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <mask
        id='mask0_93_3097'
        maskUnits='userSpaceOnUse'
        x='0'
        y='0'
        width={dimension}
        height={dimension}
      >
        <path d='M0 3.8147e-06H40V40H0V3.8147e-06Z' fill='white' />
      </mask>
      <g mask='url(#mask0_93_3097)'>
        <path
          d='M23.5255 9.85027H35.4688C37.5398 9.85027 39.2188 11.5292 39.2188 13.6003V33.9063C39.2188 35.9773 37.5398 37.6563 35.4688 37.6563H4.53125C2.46016 37.6563 0.78125 35.9773 0.78125 33.9063V13.6003C0.78125 11.5292 2.46016 9.85027 4.53125 9.85027H16.4943'
          stroke={color}
          strokeWidth='2'
          strokeMiterlimit='10'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <path
          d='M39.2188 16.0916C39.2188 20.2217 35.8706 23.5699 31.7405 23.5699H8.25953C4.12937 23.5699 0.78125 20.2217 0.78125 16.0916'
          stroke={color}
          strokeWidth='2'
          strokeMiterlimit='10'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <path
          d='M20.0098 28.4375C18.3702 28.4375 17.041 27.1084 17.041 25.4688V21.1279C17.041 20.5769 17.4877 20.1302 18.0387 20.1302H21.9809C22.5319 20.1302 22.9785 20.5769 22.9785 21.1279V25.4688C22.9785 27.1084 21.6494 28.4375 20.0098 28.4375Z'
          stroke={color}
          strokeWidth='2'
          strokeMiterlimit='10'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <path
          d='M10.3125 9.85028V6.0938C10.3125 4.0227 11.9914 2.3438 14.0625 2.3438H25.9375C28.0086 2.3438 29.6875 4.0227 29.6875 6.0938V9.85028'
          stroke={color}
          strokeWidth='2'
          strokeMiterlimit='10'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <path
          d='M10.3125 6.09375H29.6875'
          stroke={color}
          strokeWidth='2'
          strokeMiterlimit='10'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
      </g>
    </svg>
  )
}

export default TaskSuitcaseIcon
