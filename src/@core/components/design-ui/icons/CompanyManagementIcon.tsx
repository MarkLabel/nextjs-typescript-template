import { useTheme } from '@mui/material/styles'

type SvgIconProps = {
  dimension?: number
  color?: string
}

const CompanyManagementIcon = (props: SvgIconProps) => {
  const theme = useTheme()
  const dimension = props.dimension ?? 24
  const color = props.color ?? theme.palette.grey[600]

  return (
    <svg
      width='24'
      height='24'
      viewBox='0 0 24 24'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      className='UiSvgOutlined'
    >
      <path
        d='M2 21.5H21.5'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M2.9355 21.5L2.9678 9.96007C2.9678 9.39038 3.31275 8.8089 3.781 8.43543L10.7846 2.9562C11.5586 2.3385 12.4414 2.3385 13.2154 2.9562L20.218 8.4285C20.6862 8.80198 21.0312 9.39038 21.0312 9.96007V21.5'
        stroke={color}
        strokeWidth='1.5'
        strokeLinejoin='round'
      />
      <path
        d='M8.5025 11.0034C7.6925 11.0034 7 11.696 7 12.5059V21.5H16.5V12.5059C16.5 11.696 15.8075 11.0034 14.9975 11.0034H9.5025H8.5025Z'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M10 16.2195V17.816'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M10.5 7.5H13.5'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  )
}

export default CompanyManagementIcon
