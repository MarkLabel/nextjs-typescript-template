import { useTheme } from '@mui/material/styles'

type SvgIconProps = {
  dimension?: number
  color?: string
}

const MyFileIcon = (props: SvgIconProps) => {
  const theme = useTheme()
  const dimension = props.dimension ?? 24
  const color = props.color ?? theme.palette.grey[600]

  return (
    <svg
      width={dimension}
      height={dimension}
      viewBox={`0 0 24 24`}
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      className='UiSvgOutlined'
    >
      <path
        d='M21.2503 19.2874C21.1046 20.7612 21.0003 21.8331 18.2884 21.8331H5.71541C3.0035 21.8331 2.89921 20.7612 2.75349 19.2874L2.4882 14.2913C2.4273 13.4291 2.6176 12.7779 2.9784 12.2885C2.98893 12.2765 2.98893 12.2644 2.99946 12.2524C3.67816 11.4431 4.37727 11.0003 5.3161 11.0003H18.6877C19.6265 11.0003 20.3208 11.4431 20.9947 12.2377C21.0052 12.2498 21.0157 12.2619 21.0262 12.2739C21.4074 12.7634 21.5978 13.4146 21.5369 14.2768L21.2503 19.2874Z'
        stroke={color}
        strokeWidth='1.5'
      />
      <path
        d='M3.5026 11.4308V7.94008C3.5026 2.89469 4.32534 2.072 7.7354 2.072H9.02162C10.3078 2.072 10.5587 2.41232 11.0719 3.0759L12.3581 4.88433C12.655 5.30192 12.8125 5.50162 13.7074 5.50162H16.3158C19.726 5.50162 20.5486 6.32428 20.5486 9.73435V11.4986'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M9.42645 17.0271H14.5731'
        stroke={color}
        strokeWidth='0.875'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  )
}

export default MyFileIcon
