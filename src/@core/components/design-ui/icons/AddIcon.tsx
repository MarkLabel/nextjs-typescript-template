import { useTheme } from '@mui/material/styles'

type SvgIconProps = {
  dimension?: number
  color?: string
}

const AddIcon = (props: SvgIconProps) => {
  const theme = useTheme()
  const dimension = props.dimension ?? 24
  const color = props.color ?? theme.palette.grey[600]

  return (
    <svg
      width={dimension}
      height={dimension}
      viewBox={`0 0 24 24`}
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M6 12H18'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M12 18V6'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  )
}

export default AddIcon
