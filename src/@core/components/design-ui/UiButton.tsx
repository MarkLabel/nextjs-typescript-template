import React from 'react'
import Button, { ButtonProps } from '@mui/material/Button'
import { styled, useTheme } from '@mui/material/styles'

const UiButton = (props: ButtonProps) => {
  const theme = useTheme()

  const StyledDesignUiButton = styled(Button)<ButtonProps>(({ theme }) => ({
    border: 0,
    '& .MuiTypography-root': {
      color: '#5895FF'
    },
    '& .MuiButton-startIcon *': {
      stroke: '#5895FF',
      color: '#5895FF'
    },
    '&:hover': {
      '& .MuiTypography-root': {
        color: theme.palette.common.white
      },
      '& .MuiButton-startIcon *': {
        stroke: theme.palette.common.white,
        color: theme.palette.common.white
      },
      backgroundColor: `'#5895FF' !important`
    }
  }))

  return <StyledDesignUiButton {...props} />
}

export default UiButton
