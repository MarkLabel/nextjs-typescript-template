/* eslint-disable react/display-name */
// ** React Imports
import { forwardRef, Ref } from 'react'

// ** MUI Imports
import {
  DataGrid,
  DataGridProps,
  GridColumns,
  GridRenderCellParams
} from '@mui/x-data-grid'

// ** Types
import { CustomDatagridProps } from './types'
import { useTheme } from '@mui/material/styles'

const UiDesignDataGrid = forwardRef(
  (props: CustomDatagridProps, ref: Ref<any>) => {
    // ** Props
    const { sx, skin, color } = props

    // ** Hook
    const theme = useTheme()

    return (
      <DataGrid
        ref={ref}
        {...props}
        sx={{
          ...sx,
          '& .MuiDataGrid-columnHeaders': {
            borderRadius: '.5rem',
            backgroundColor: 'rgba(88,149,255, .125)',
            fontWeight: 'bold',
            borderColor: 'transparent',
            color: '#64a5ff',
            '& .MuiDataGrid-columnHeaderTitle': {
              fontWeight: '700',
              fontSize: '.85rem'
            }
          },
          '& .MuiDataGrid-iconSeparator': {
            color: 'transparent'
          },
          '& .MuiDataGrid-row:hover': {
            backgroundColor: '#f7f9fd',
            borderRadius: '.5rem',
            cursor: 'pointer',
            '& .MuiTypography-root': {
              color: '#64a5ff'
            }
          }
        }}
      />
    )
  }
)

export default UiDesignDataGrid
