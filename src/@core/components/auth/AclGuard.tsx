// ** React Imports
import { ReactNode, useState, useEffect } from 'react'

// ** Next Import
import { useRouter } from 'next/router'

// ** Types
import type { ACLObj, AppAbility } from 'src/configs/acl'

// ** Context Imports
import { AbilityContext } from 'src/layouts/components/acl/Can'

// ** Config Import
import { buildAbilityFor } from 'src/configs/acl'

// ** Component Import
import NotAuthorized from 'src/pages/401'
import Spinner from 'src/@core/components/spinner'
import BlankLayout from 'src/@core/layouts/BlankLayout'

// ** Hooks
import { useAuth } from 'src/hooks/useAuth'

// ** Util Import
import getHomeRoute from 'src/layouts/components/acl/getHomeRoute'

interface AclGuardProps {
  children: ReactNode
  authGuard?: boolean
  guestGuard?: boolean
  aclAbilities: ACLObj
}

const AclGuard = (props: AclGuardProps) => {
  // ** Props
  const { aclAbilities, children, guestGuard = false, authGuard = true } = props

  // ** Hooks
  const auth = useAuth()
  const router = useRouter()

  const [ability, setAbility] = useState<AppAbility | undefined>(undefined)

  if (auth.user != null && ability == null) {
    setAbility(buildAbilityFor('admin', aclAbilities.subject))
  }

  // If guest guard or no guard is true or any error page
  if (
    guestGuard ||
    router.route === '/404' ||
    router.route === '/500' ||
    !authGuard
  ) {
    // If user is logged in and his ability is built
    return <>{children}</>
  }

  // Check the access of current user and render pages
  if (
    ability != null &&
    ability.can(aclAbilities.action, aclAbilities.subject)
  ) {
    return (
      <AbilityContext.Provider value={ability}>
        {children}
      </AbilityContext.Provider>
    )
  }
  // Render Not Authorized component if the current user has limited access
  return (
    <AbilityContext.Provider value={undefined}>
      {children}
    </AbilityContext.Provider>
  )
}

export default AclGuard
