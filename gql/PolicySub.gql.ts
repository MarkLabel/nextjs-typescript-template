import { gql } from '@apollo/client'

export const gqlPolicySub = gql`
  subscription Policy($policyId: Int!) {
    policy(policyId: $policyId) {
      coverageTokenId
      expiryTimestamp
      issueTimestamp
      multiplier
      name
      eventName
      canClaim
      openInterest
      policyAddress
      policyId
      policyUri
      premiumTokenId
      settlementToken
      settlementTokenAddress
      underwriteAddress
      underwriteId
      underwriteUri
    }
  }
`
